let adiv = document.querySelector('#answer')
let qdiv = document.querySelector('#question')
let ldiv = document.querySelector('#log')

let answers = [
    'chances are higher than the Burj Khalifa',
    'yes, but i cant say it will happen',
    'It might happen, it might not, but there is a high chance',
    '60-40',
    'fifty fifty chance',
    '*ahem* hey, can you speak up? i cant hear you...',
    'low chances, but still possible.',
    'you would be lucky to have this happen at all',
    'idk what to say, other than "its not gonna happen"',
    'NOW you would have to be lucky.',
    'forget about it. please just forget about it.',
]
    
 function randomAnswer() {
     let r = Math.random()
     let n = r * answers.length
     n = Math.floor(n)
     return answers[n]
 }  
 qdiv.onkeypress = e =>{
     if (e.key === 'Enter')  {
        adiv.innerHTML = randomAnswer()
        log()
        qdiv.innerHTML = ''
        return false
     }
 }



function log() {
    let entry = document.createElement('div')
    let q = document.createElement('div')
    q.innerText  = qdiv.innerText
    let a = document.createElement('div')
    a.innerText  = adiv.innerText
    entry.appendChild(q)
    entry.appendChild(a)
    ldiv.insertBefore(entry,ldiv.children[0])
} 
    





adiv.innerHTML = randomAnswer()