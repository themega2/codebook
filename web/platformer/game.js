let pl

class Main extends Phaser.Scene {
    preload() {
        this.load.image('bg', 'assets/desktop.png')
        this.load.image('pl','assets/player.png')

    }
    create() {
        this.add.image(0, 0, 'bg').setOrigin(0, 0)
        pl=this.physics.add.sprite(79,93,'pl')
        pl.setGravityY(200)
        pl.setCollideWorldBounds(true)
    }
}

let game = new Phaser.Game({
physics: {default: 'arcade'} ,

    scene: Main
})